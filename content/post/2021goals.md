+++
title = "Is this a better way of doing New Year's Resolutions?"
author = ["Jack Cryer"]
publishDate = 2020-12-30
draft = false
+++

## New Year, New Me {#new-year-new-me}

As the year draws to a close, it's once again time for everyone to consider what changes they want to make in their lives.
I'm sure that nobody could have predicted how 2020 would have panned out.
Most people probably found themselves with more free time than they had in previous years.
I know I did, but still I feel as though I didn't really make the best use of this free time.

Last year, my New Year's Resolutions were pretty generic.
Read more.
Do more exercise.
So on and so forth.

I was reading everyday for the first week or so, until I went to visit the in-laws and my E-Reader ran out of battery and they didn't have any spare chargers.
After that, I more or less read three books in the whole year.
To be honest, that is probably not that different from the year before, so it could be argued that I stuck to my resolution.


## New Year's Goal System {#new-year-s-goal-system}

Thinking about this I have realised that I need to implement a whole new style of resolution.
I know that I look at numbers everywhere I go, so I think it would be best for me to set quantifiable goals.
I have heard of people "gamifying" their lives and I think that this is a great idea too.

Over the years, I have started so many projects by myself, but I have given up after I lose interest.
Without trying to sound too "Fifty Shades of Grey", I have many uncommon interests.
Most of them are the sort of thing I can't start a conversation about down the pub.
I plan to post occasional updates about my goals on this blog.
I want to be able to use this new system to keep me on my toes.


## Point System {#point-system}

I have come up with a system in which I have 100 points to be awarded for completing various tasks within certain categories.
Perhaps I will place bets with family and friends about how many points I can achieve before then end of the year.


### Reading {#reading}

I have probably read four or five books cover to cover each year for the past few years.
It's not great but it's probably not uncommon either.


#### Points (30 points) {#points--30-points}

My goal for 2021 is to read at least 24 books, with at least 6 of those being Chinese.
2 books a month doesn't sound too bad does it.
I don't have to read one book every two weeks.
I could read 6 Chinese books starting January 1st, and then spend the rest of the year reading English books to make up the slack.

-   1 point for every English book.
-   2 points for every Chinese book.

I may award some bonus points, but I haven't really thought about how I'll do that yet.


### Blog Posts {#blog-posts}

This is the first post on this blog, but I have been working on, or more accurately, putting off this blog for a long time now.
Procrastination is one of my worst habits, and it's something I need to work on.
I hope to be able to build a good writing habit over this year and to carry it into the future.


#### Points (30 points) {#points--30-points}

My goal for 2021 is to write at least 24 blog posts, with at least 6 of those being Chinese.

-   1 point for every English blog post.
-   2 points for every Chinese blog post.

24 blog posts is actually not that many.
I follow people who post more than that in a month.
As this is my first year doing any significant writing for a long time, I decided to aim low and if I surpass it, that's great.

I am almost certain that I will not be awarding any bonus points for this section.


### Exercise {#exercise}

For my exercise portion, I want to split this into two points.


#### Football (10 points) {#football--10-points}

My goal for 2021 is to score 100 goals in all competitions.
I play a 5-a-side or 7-a-side match a few times a month and a casual kickabout most weekends.

-   1 point for every 10 goals scored

Is 100 goals in a year reasonable?
It's definitely achievablie.
I scored four last night.
Maybe I shouldn't be including goals scored in my kickabout, but if Pelé can do it, so can I.


#### Other exercise (10 points) {#other-exercise--10-points}

I want to take up a new sport in the new year, but I haven't decided what I want to do yet.
Because of this, I will leave this section blank for now.
I will update this post once I have decided.


### Programming {#programming}

I have been learning to program for the last year.
The language I have been learning is Python, which has many usecases.

Again, my procrastination has been a problem for me, so I want to give myself some meaningful objectives.


#### Points (10 points) {#points--10-points}

-   5 points for writing a useful, graphical application for Linux
-   3 points if I receive a pull request
-   2 points if a project of mine receives 10 stars

These are tasks that rely on other people, so I'm not sure if they will actually be completed.
I have started using git lately.
In fact, this blog is the first git project I have published


### Private {#private}

While I think it's a good idea to post my objectives publicly so that people can keep me on the right path, I am still a very private person and like to keep a few of these points for private tasks that I am not yet ready to reveal.
When I eventually do an end of year review, I should be able to share these tasks with you then.


## Conclusion {#conclusion}

| Task                        | Points |
|-----------------------------|--------|
| Read 18 English books       | 18     |
| Read 6 Chinese books        | 12     |
| Write 18 English blog posts | 18     |
| Write 6 Chinese blog posts  | 12     |
| Score 100 goals             | 10     |
| TBD                         | 10     |
| Write a Linux application   | 5      |
| Receive a pull request      | 3      |
| Get 10 stars                | 2      |
| Private                     | 10     |

Hopefully, having published this blog post, my friends and family will also be able to hold me accountible.
I expect my friends to ask how many goals I have scored this year at football, or for a general update.

I feel optimistic about this upcoming year, and I think that having a system like this in place will help keep me from getting distracted.
Perhaps, this time next year, I will be doing a similar post and implementing improvements to this system.
