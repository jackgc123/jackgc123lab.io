+++
title = "I'm Engaged"
author = ["Jack Cryer"]
publishDate = 2021-01-16
draft = false
+++

## What a time to be alive {#what-a-time-to-be-alive}

Personally, my 2020 wasn't that bad to be honest.
I was in the UK when China was in lockdown and then I came back to China just before the UK went into lockdown.
Sherry and I spent a total of two weeks in quarantine when we arrived back in Chengdu back in March and our lives have been pretty much back to normal since April.
I went back to work at the end of May and everything was more or less the same as before we left.

With all this said, I want to make 2021 even better, and hopefully it can be my best year ever.
To start the year off with a bang, the love of my life agreed to marry me on January 16th.
I had been planning my proposal for a few months now and had six amazing people help me bring my ideas to life even better than I could have imagined.


## The evening {#the-evening}

On the evening of the night, we went for dinner with our friends to a nice western restaurant near Chunxi Road.
I was a nervous wreck the whole time, but our friends told me that they couldn't tell at all.
I guess I should be proud that I could hold myself together.

While we were out, I gave Sherry's parents the key to our appartment as I wanted them and Sherry's two high school friends to decorate when we were out for dinner.
I asked them to hang up a winter themed background image that I had ordered online.
My original plan was for the proposal to take place outside, but once my background arrived, I realised how difficult it would be to hang it up in the spot where I had originally planned to propose.
This turned out to be very fortunate as it happened to rain on the evening.

I also asked them to blow up some baloons to decorate, and they well and truly blew my expectations out of the water.
Sherry's parents must have blown up over 100 baloons and made a beautiful pathway in the hall as well as a heart made of baloons in the living room.
However, the part of my plan I knew that Sherry would love the most, and she later confirmed it to be true, was the custom clothes I ordered for our two dogs with the words "Marry" and "Me" printed on them respectively.

{{< figure src="/images/002.jpg" >}}


## The Proposal {#the-proposal}

We arrived back home at about 8pm and found that the door had been left open.
Sherry opened the door, saw the decorations and saw the two dogs running towards her down a baloon path with clothes which said "Marry Me" printed on them.
We both stood there for a few minutes in shock before we walked into the living room where Sherry's parents and two of her friends were waiting for us.

{{< figure src="/images/003.jpg" >}}

I got down on one knee and did my speech in Chinese.
I had been so busy all day that I didn't really have time to write and memorise anything, so I just spoke from my heart.

> 宝宝，我们现在在一起快四年了。在我们在一起的第一个月我就知道你是我的“那个人”。在第一个月我就知道我想娶你。在第一个月我就知道我想永远和你在一起。你是我最好的朋友。你是我最爱的人。我现在根本想不到一个没有你的生活。跟你在一起，我天天想变得更好，是因为你太好了。我非常非常爱你。你愿意嫁给我吗？

Which in English is something like this:

> Baby, we have been together nearly four years now, but in the first month I knew that you were my soulmate. In the first month, I knew that I wanted to marry you. In the first month, I knew that I wanted to spend the rest of my life with you. You're my best friend and you are the love of my life. I can't even begin to imagine what my life would be like without you. Everyday that we are together, you make me want to become a better person. You are perfect for me and I love you more than anything in the world. Will you marry me?

To my relief, she said yes.
We then took some pictures with family, friends and the dogs, and we opened a lovely bottle of sparkling wine.
It was a night that I will never forget and I feel so emotional everytime I think about it.

{{< figure src="/images/004.jpg" >}}


## Final thoughts {#final-thoughts}

This is something that I had been planning for a long time, so I am extremely happy that I could pull it off without any major issues.
I am very thankful for all the support and kind messages from those who helped in the planning and also those who congratulated us after.
Finally, I would like to say that I am very grateful to Sherry, not just for saying yes on the night, but for saying yes when we first started dating and for tolerating me everyday.

{{< pixelfed id="jackgc123/256639554902167552" >}}

You can view more photos on the Pixelfed post I have embedded above.
