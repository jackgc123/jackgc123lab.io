+++
title = "January 2021"
author = ["Jack Cryer"]
publishDate = 2021-02-05
lastmod = 2021-02-06T16:46:15+08:00
draft = false
+++

Frequent readers of my blog may remember that in my [new year's goals](/post/2021goals) post, I set a rather lofty target for myself.
The day before I wrote that post, I had just played a very casual kickabout with the guys and had scored 7 goals if memory serves correctly.

This year seems to be going a little bit differently so far.
I haven't played a midweek match and the matches I have played have been more competitive.

Enough with my excuses, you are all here to see how marvellously (pathetically) I fail at this challenge.
I will be posting monthly updates shortly after each month concludes.
You can find these posts at <https://jackgc123.gitlab.io/football>

| Month   | Football Sessions | Goals |
|---------|-------------------|-------|
| January | 2                 | 2     |

If I keep up at this rate, I won't even be hitting a quarter of my targetted goals.
Admittedly, I missed the first Sunday session as it was over the New Year's holiday and I was staying at Sherry's parents
However, the biggest reason for my lack of play time was that I suffered a toe injury that kept me out for three weeks.


## Sunday, January 10th 2021 - 0 goals {#sunday-january-10th-2021-0-goals}

I was very excited for my first match of the year.
Having scored 7 goals in the final match of 2020, I was eager to continue my fine goalscoring form into the New Year.
Those of you who have played football with me before, will have a certain preconceived image of the type of player I am.
Forza BJFC!
But, my game has evolved since the days of playing John, Gordon, Ali and the boys at the Fenham Cage.

Nowadays, I can often be seen leading the line for my side.
I'm either setting the tempo for the team's press, or I'm providing an option for our defenders to go long to.
But on the night of my first match of 2021, I couldn't do either of these effectively.

It started out with two fairly unbalanced sides.
I was our furthest player forward, but I probably only had about five shots before being forced off through injury.
No one on the team had a particularly bad game, the teams were just uneven from the beginning.

Personally, besides my poor finishing when the rare chance presented itself, the other areas of my game were quite good on the night.
My personal highlight was perhaps the greatest recovery run that Chengdu has ever seen.
I noticed that there was an attacker loitering on the right hand side of our box unmarked and I sprinted 75% of the pitch and made a calm interception just before the low drilled cross met its target.

My best chance to score was actually what led to me colliding toes with their goalkeeper and bruising my big toe just after the hour mark.
We had just conceded and I was taking the kick-off.
I layed the ball off to our midfielder who returned it to me as I passed their first line of defence.
There was little the next defender could do as I peeled of to the left before quickly turning back inside.

It kind of became a blur at this point.
I know that I beat their last defender and made my way into the box, but I let the ball get to close to the goalkeeper.
He was quick off his line and he managed to clear the ball and my big toe with a powerful kick.

As much as the sheer determination to score goals urged me on, the inability to run or kick the football was ultimately too much for me this day and I went home early, sulking all the way home.


## Sunday, January 31st 2021 - 2 goals {#sunday-january-31st-2021-2-goals}

To welcome the Chinese New Year, it was decided that the final match of January would be a four-way tournament.
I ended up on a team called whose name was either "The Underdogs" or "Big Dogs", depending on who you asked.
We went into the tournament quite literally as underdogs, so there was no pressure or expectations on us.
We were free to go out and express ourselves on the pitch.


#### Big Dogs vs Angels 1-4 {#big-dogs-vs-angels-1-4}

Our first match was against the bookies' favourites and eventual champions, Angels.
We started the game defending well and were unlucky to concede an early goal.
Great character was shown by all as we kept our heads up.
Shortly after, great pressure on the opposing goalkeeper from yours truly, resulted in us pegging one back.

For the next five or ten minutes, the match was even.
I put a tasty in swinging corner into the box and the big centre back for Angels, I think his name might have been Gabriel or something, managed to shoulder it onto the far post, where it bounced off directly to our attacker who hit the crossbar but couldn't keep it under.
The woodwork was rattled twice in five seconds.

Eventually, we ended up capitulating and losing the game 4-1, but we can take pride in the fact that it was only the quality of our finishing between the two sides on the day.


#### Big Dogs vs Englads 1-4 {#big-dogs-vs-englads-1-4}

Next up was our match against Englads with special guest English Dave.
This is a team that has been playing together week in week out for at least 60 years.
A more appropriate name might have been EngOAPs.

This was another match of close margins.
Two wonderstrikes from the experienced team aside, it was a close but enjoyable match in which I personally did little that warrants writing in this post.


#### Big Dogs vs La Dance 6-8 {#big-dogs-vs-la-dance-6-8}

Up until this point, we had a grand total of 0 points in the tournament and some lesser teams might have just downed tools, but not the Big Dogs.
Before this match had even begun, there was cause to be concerned going into the final match.
We had heard whispers from the Englads team, that La Dance had been cheating in their first match.
I can tell you that I speak on behalf of all the Big Dogs players when I say there is no room for cheating in the Gentleman's Football Federation 1st Annual CNY 5-a-side Tournament brought to you in association with Rob's MVP Pizza.

I started the game in between the sticks and I was shouting very passionately at all of my teammates from as soon as the whistle went.
La Dance settled into the game quickly and managed to get a few past me.
Were the Big Dogs players tired after playing two matches?
Perhaps they could conjure up the energy to take the game back to La Dance.

About five minutes later, I'd just come outfield and I'm playing on the left.
The ball comes to me and I quickly tap it past the oncoming defender.
I feigned going inside and pulled the ball to my left leaving the next defender with nothing he could do.
As a team, our finishing had been poor all night, but could I put this ball away when it really mattered?

I fired a cheeky right footed shot through the keeper's legs as the crowd went wild.
We did actually have a group of supporters and they thoroughly enjoyed it.
Unfortunately we couldn't capitalise on the goal and after the thirty minutes were up the score was supposedly 8-3.

After this, we kept on saying one more goal and we kept on scoring.
There were great team goals, great individual goals and even great goals on the counter.
Everybody managed to get in on the act.

Eventually we had to call time on the game and La Dance reported the score as 8-3 to the organisers, saying that all goals scored after some arbitrary point they decided without telling us shouldn't count to the score as we were just having a kickabout while we waited for the other match to end.
I'm not sure what the real final score should have been, but we definitely had more than 3 goals.
If we had played the way we did at the end for the whole duration of the match, who knows what the result would have been.
Too little too late, I guess.

Overall, I'm disappointed with my goal talley for the month and I can tell you now that next month will probably be even worse.
I am staying at Sherry's parents for most of the month and will probably only be able to play one match on the final Sunday of February.
If I can score 14.6666 goals in that match, it should get me back on target for 100 goals in 2021.
