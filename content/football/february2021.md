+++
title = "February 2021"
author = ["Jack Cryer"]
publishDate = 2021-03-05
lastmod = 2021-03-05T22:32:06+08:00
draft = false
+++

Frequent readers of my blog may remember that in my [new year's goals](/post/2021goals) post, I set a rather lofty target for myself.
The day before I wrote that post, I had just played a very casual kickabout with the guys and had scored 7 goals if memory serves correctly.

This year seems to be going a little bit differently so far.
I haven't played a midweek match and the matches I have played have been more competitive.

Enough with my excuses, you are all here to see how marvellously (pathetically) I fail at this challenge.
I will be posting monthly updates shortly after each month concludes.
You can find these posts at <https://jackgc123.gitlab.io/football>

| Month    | Football Sessions | Goals |
|----------|-------------------|-------|
| January  | 2                 | 2     |
| February | 2                 | 6     |
| Total    | 4                 | 8     |

A much better performance in February, even though I didn't play any football until the month was almost over.
6 goals in a week.
If I can keep it up at this rate, I'll be done well before the summer.


## Wednesday, February 25th 2021 - 3 goals {#wednesday-february-25th-2021-3-goals}

My return to the hallowed pitch was on an unexpected Wednesday morning.
We were planning to stay at Sherry's parents for the whole month, but I was getting a bit sick of all the Chinese new year hospitalities, so we made an excuse to go back early.

Whilst I was well rested over the holiday, most of my friends were not.
They were playing at least twice a week and had competitive tournaments as well.
Would my fresh legs prove to be advantageous on this chilly Wednesday morning?
Not really.
My legs were mostly full of hot pot and Chinese baijiu.

The match started off evenly and I got off the mark quite early.
Great movement and runs are not attributes that I typically associate with my general style of play.
I made a clever in to out run, running from the centre of the pitch to the right, as I slipped behind the opposing defender.
The run was spotted by our team's midfielder who made an inch perfect throughball that I latched onto before slotting the ball in the far corner with a first time finish.

The next goal was perhaps a bit more lucky, but it was a good finish nontheless.
This time I held the ball up on the right wing before laying it off to a teammate who feigned turning backwards, drawing the opposing midfielder before disguising a backheel return ball back up the right wing, catching the opposition and me offguard.
I managed to get a hold of the ball before the defender could catch up with me, and at this point I really noticed how leggy the rest of the guys were, leaving me with only one option which was to go for goal myself.
My shot almost resembled a cross in technique, but it managed to beat the keeper at his near post.

As the game went on, it was impossible for me to avoid feeling just as leggy as the rest of my teammates, and it's fair to say that there was a noticeable drop in quality as the game went on.
I feel as though my third goal doesn't even warrant me taking time to write about.

Basically, both teams were tired and there wasn't much tracking back, and I scored a sweaty goal from 3 yards out.
I hope you enjoyed reading that.


## Sunday, February 28th 2021 - 3 goals {#sunday-february-28th-2021-3-goals}

My second match in February was quite uneventful despite scoring three goals.
We had 18 players, and because people in the group decided they didn't want to rotate three teams of six on one pitch, we ended up playing a 5v5 and a 4v4 match with rotations.
An early injury led to that basically becoming three teams of four.
For the first hour or so, we just ran about tiring ourselves out without scoring many goals.

After an hour or so, some people had had enough and decided to go home, leaving us with enough for a 6v6 match with a few subs.
I was on the bench for a while and then somebody came with a load of free pizzas provided by Rob's MVP pizza.
I had two slices and felt revitalised.
It made me wonder why pizza is never considered a halftime orange alternative.

I came on once again with a pizza induced swagger and made a quick impact.
Our defender launched a long ball over the top that I brought down before finishing with a half volley.
This goal has definitely got to be up there with the best goal that I've scored this year.
So far.

The next two goals were quite standard goals an to be honest, I don't really remember any particular details of them.
The three goals I scored at the end of the match took place in the space of about five minutes.

The pizza might have had some drug-like properties as it enhanced my game, while also making me unable to remeber what happened.


## Conclusion {#conclusion}

Despite only playing two matches, february was a much better month than I expected.
March will hopefully be even better as I hope it will be the first time when I can consistently play football for a whole month.

I am much more optimistic that I can reach my target now than I was when I wrote my last update.
When I came up with my original new year's goals, I thought that this would be the hardest of all of my goals.
However, this is something that as long as I go to football every week, the number should at least go up.

I need to work this month on the number of books I'm reading and sit down and plan out and write a few blog posts too.
